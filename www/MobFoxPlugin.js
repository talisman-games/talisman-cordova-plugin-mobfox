var exec = require('cordova/exec');

function MobFoxPlugin() {
  console.log("MobFoxPlugin.js: is created");
}

MobFoxPlugin.prototype.showBanner = function (hash, position, bannerWidth, bannerHeight) {
  if (typeof hash === 'undefined') hash = 'fe96717d9875b9da4339ea5367eff1ec';
  if (typeof position === 'undefined') position = 'bottom-center';
  // supported vertical position values: 'top', 'bottom'
  // supported horizontal position values: 'left', 'center', 'right'
  if (typeof bannerWidth === 'undefined') bannerWidth = 320;
  if (typeof bannerHeight === 'undefined') bannerHeight = 50;

  var onSuccess = function (result) {
    var myCustomEvent = new Event(result);
    document.dispatchEvent(myCustomEvent);
  };
  var onFailure = function (result) {
    var myCustomEvent = new CustomEvent("onBannerFailed", {
      detail: {
        message: result,
        time: new Date()
      },
      bubbles: true,
      cancelable: true
    });
    document.dispatchEvent(myCustomEvent);
  };

  exec(onSuccess, onFailure, "MobFoxPlugin", "showBanner", [{
    hash: hash, position: position, bannerWidth: bannerWidth, bannerHeight: bannerHeight
  }]);
};

MobFoxPlugin.prototype.hideBanner = function () {
  exec(function (result) {}, function (result) {}, "MobFoxPlugin", "hideBanner", []);
};

MobFoxPlugin.prototype.unhideBanner = function () {
  exec(function (result) {}, function (result) {}, "MobFoxPlugin", "unhideBanner", []);
};

var mobFoxPlugin = new MobFoxPlugin();
module.exports = mobFoxPlugin;
