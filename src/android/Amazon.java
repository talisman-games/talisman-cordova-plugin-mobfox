// Amazon.java
// Written by Andre Arsenault on 20/11/2017
// andre@talisman.games

package com.mobfox.sdk.customevents;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.widget.FrameLayout;
import com.amazon.device.ads.Ad;
import com.amazon.device.ads.AdError;
import com.amazon.device.ads.AdLayout;
import com.amazon.device.ads.AdProperties;
import com.amazon.device.ads.AdRegistration;
import com.amazon.device.ads.AdSize;
import com.amazon.device.ads.AdTargetingOptions;
import com.amazon.device.ads.DefaultAdListener;
import java.util.Map;

public class Amazon extends DefaultAdListener implements CustomEventBanner {
  private static final String TAG = "MobFoxPlugin";
  private static final boolean TEST_MODE = false;
  private CustomEventBannerListener mListener = null;
  private Context mContext = null;
  private AdLayout mBanner = null;

  @Override
  public void loadAd(Context context, CustomEventBannerListener listener, String networkID,
      Map<String, Object> params) {
    testLogDebug("MobFox Amazon Custom >> loadAd()");

    mContext = context;
    mListener = listener;
    mBanner = createBanner(networkID, params);
    requestBannerAd();
  }

  @Override
  public void setListener(CustomEventBannerListener listener) {
    testLogDebug("MobFox Amazon Custom >> setListener(listener: " + listener + ")");
    mListener = listener;
  }

  private AdLayout createBanner(String networkID, Map<String, Object> params) {
    testLogDebug("MobFox Amazon Custom >> createBanner()");

    AdRegistration.setAppKey(networkID);
    if (TEST_MODE) {
      AdRegistration.enableTesting(true);
      AdRegistration.enableLogging(true);
    }

    AdLayout banner = new AdLayout((Activity) mContext, AdSize.SIZE_AUTO);
    banner.setListener(this);
    banner.disableAutoShow(); // prevents loadAd() from blocking the UI thread under some
                              // circumstances
    // See:
    // https://forums.developer.amazon.com/questions/39476/help-with-android-banner-ad-anrs.html

    // TODO: Get the gravity from MobFoxPlugin, which already calculated it.
    final FrameLayout.LayoutParams layout = new FrameLayout.LayoutParams(
        FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
    layout.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL; // gravityFromPosition(position);
    banner.setLayoutParams(layout);

    return banner;
  }

  private void requestBannerAd() {
    testLogDebug("MobFox Amazon Custom >> requestBannerAd()");
    AdTargetingOptions options = new AdTargetingOptions();
    options.enableGeoLocation(true);
    if (!mBanner.loadAd(options)) {
      testLogWarn("MobFox Amazon Custom >> loadAd() returned false");
    }
  }

  @Override
  public void onAdLoaded(Ad ad, AdProperties adProperties) {
    testLogDebug("MobFox Amazon Custom >> onAdLoaded()");
    if (mBanner.showAd()) {
      mListener.onBannerLoaded((AdLayout) ad);
    } else {
      testLogWarn(
          "MobFox Amazon Custom >> The ad was not shown. Check the logcat for more information.");
    }
  }

  @Override
  public void onAdFailedToLoad(Ad ad, AdError error) {
    testLogError("MobFox Amazon Custom >> onAdFailedToLoad() - error: " + error.getCode()
        + ", message: " + error.getMessage());
    mListener.onBannerError((AdLayout) ad,
        new Exception("errorCode: " + error.getCode() + " errorMsg: " + error.getMessage()));
  }

  @Override
  public void onAdExpanded(Ad ad) {
    testLogDebug("MobFox Amazon Custom >> onAdExpanded()");
    mListener.onBannerClicked((AdLayout) ad);
  }

  @Override
  public void onAdCollapsed(Ad ad) {
    testLogDebug("MobFox Amazon Custom >> onAdCollapsed()");
    mListener.onBannerClosed((AdLayout) ad);
  }

  @Override
  public void onAdDismissed(Ad ad) {
    testLogDebug("MobFox Amazon Custom >> onAdDismissed()");
  }

  private static void testLogDebug(String msg) {
    if (TEST_MODE) {
      Log.d(TAG, msg);
    }
  }

  private static void testLogWarn(String msg) {
    if (TEST_MODE) {
      Log.w(TAG, msg);
    }
  }

  private static void testLogError(String msg) {
    if (TEST_MODE) {
      Log.e(TAG, msg);
    }
  }
}
