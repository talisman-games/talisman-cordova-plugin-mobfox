import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.mobfox.sdk.banner.Banner;
import com.mobfox.sdk.banner.Banner.Listener;
import com.mobfox.sdk.networking.MobfoxRequestParams;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MobFoxPlugin extends CordovaPlugin {
  public static final String TAG = "MobFoxPlugin";
  private static final boolean TEST_MODE = false;
  private static final String TESTING_HASH = "fe96717d9875b9da4339ea5367eff1ec";
  private Banner mBanner = null;
  private CallbackContext mBannerCallback = null;
  private Context mContext = null;

  // #############################################################
  // ### C O R D O V A ###
  // #############################################################

  @Override
  public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    super.initialize(cordova, webView);
    testLogDebug("Init MobFoxPlugin");
    mContext = cordova.getActivity();
    setGDPRData();
  }

  @Override
  public boolean execute(final String action, JSONArray args, CallbackContext callbackContext)
      throws JSONException {
    if (action.equals("showBanner")) {
      mBannerCallback = callbackContext;
      JSONObject dict = new JSONObject(args.getString(0));
      final String hash = TEST_MODE ? TESTING_HASH : dict.getString("hash");
      final String position = dict.getString("position");
      final int bannerWidth = dict.getInt("bannerWidth");
      final int bannerHeight = dict.getInt("bannerHeight");

      cordova.getActivity().runOnUiThread(
          () -> showBanner(hash, position, bannerWidth, bannerHeight));
      return true;
    }

    if (action.equals("hideBanner")) {
      mBannerCallback = callbackContext;
      cordova.getActivity().runOnUiThread(this::hideBanner);
      return true;
    }

    if (action.equals("unhideBanner")) {
      mBannerCallback = callbackContext;
      cordova.getActivity().runOnUiThread(this::unhideBanner);
      return true;
    }

    Log.e(TAG, "Unexpected plugin action: " + action);
    return false;
  }

  private void sendCallback(CallbackContext trgCallback, boolean bOK, String text) {
    if (trgCallback != null) {
      PluginResult pluginResult;
      if (bOK) {
        pluginResult = new PluginResult(PluginResult.Status.OK, text);
      } else {
        pluginResult = new PluginResult(PluginResult.Status.ERROR, text);
      }
      pluginResult.setKeepCallback(true);
      trgCallback.sendPluginResult(pluginResult);
    }
  }

  private void setGDPRData() {
    // Amazon MobAds GDPR legitimate interest flag.
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
    SharedPreferences.Editor editor = preferences.edit();
    editor.putString("aps_gdpr_pub_pref_li", "1");
    editor.apply();
  }

  // #############################################################
  // ### B A N N E R ###
  // #############################################################

  private int gravityFromPosition(String position) {
    // Expected values: (top|bottom)-(center|left|right)
    // Examples: bottom-center, top-left
    int g = Gravity.NO_GRAVITY;

    // Vertical properties (before the dash)
    if (position.startsWith("top")) {
      g |= Gravity.TOP;
    } else if (position.startsWith("bottom")) {
      g |= Gravity.BOTTOM;
    } else {
      Log.e(TAG,
          "gravityFromPosition: Unexpected vertical alignment parameter for position (" + position
              + "), defaulting to top.");
      g |= Gravity.TOP;
    }

    // Horizontal properties (after the dash)
    if (position.endsWith("center")) {
      g |= Gravity.CENTER_HORIZONTAL;
    } else if (position.endsWith("left")) {
      g |= Gravity.LEFT;
    } else if (position.endsWith("right")) {
      g |= Gravity.RIGHT;
    } else {
      Log.e(TAG,
          "gravityFromPosition: Unexpected horizontal alignment parameter for position (" + position
              + "), defaulting to center.");
      g |= Gravity.CENTER_HORIZONTAL;
    }
    return g;
  }

  private void showBanner(String myHash, String position, int bannerWidth, int bannerHeight) {
    if (mContext == null) {
      return;
    }

    testLogDebug("showBanner()");

    // If we don't have a banner object yet, create one. Otherwise we will just call
    // .load() on the existing banner object instead of creating a whole new
    // WebView.
    if (mBanner == null) {
      mBanner = createBanner(myHash, position, bannerWidth, bannerHeight);
    }
    mBanner.load();
  }

  private Banner createBanner(String myHash, String position, int bannerWidth, int bannerHeight) {
    testLogDebug("createBanner(position: " + position + ", bannerWidth: " + bannerWidth
        + ", bannerHeight: " + bannerHeight + ")");

    Banner banner = new Banner(mContext, bannerWidth, bannerHeight, myHash, mBannerListener);

    // GDPR consent
    MobfoxRequestParams mfrp = new MobfoxRequestParams();
    mfrp.setParam(MobfoxRequestParams.GDPR, 1);
    mfrp.setParam(MobfoxRequestParams.GDPR_CONSENT, "1");
    banner.addParams(mfrp);

    final View parentView = ((Activity) mContext).getWindow().getDecorView().getRootView();
    if (parentView != null) {
      final FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
          FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
      params.gravity = gravityFromPosition(position);
      ((ViewGroup) parentView).addView(banner, params);
    }

    return banner;
  }

  private void hideBanner() {
    testLogDebug("hideBanner()");
    if (mContext != null && mBanner != null) {
      mBanner.setVisibility(View.GONE);
    }
  }

  private void unhideBanner() {
    testLogDebug("unhideBanner()");
    if (mContext != null && mBanner != null) {
      mBanner.setVisibility(View.VISIBLE);
    }
  }

  // =======================================================================

  private Listener mBannerListener = new Banner.Listener() {
    @Override
    public void onBannerError(Banner banner, Exception e) {
      testLogError("onBannerError: " + e.getMessage());
      if (mContext != null) {
        sendCallback(mBannerCallback, false, e.getMessage());
      }
    }

    @Override
    public void onBannerLoaded(Banner banner) {
      testLogDebug("onBannerLoaded()");
      if (mContext != null) {
        sendCallback(mBannerCallback, true, "onBannerLoaded");
      }
    }

    @Override
    public void onBannerClosed(Banner banner) {
      testLogDebug("onBannerClosed()");
      if (mContext != null) {
        sendCallback(mBannerCallback, true, "onBannerClosed");
      }
    }

    @Override
    public void onBannerClicked(Banner banner) {
      testLogDebug("onBannerClicked()");
      if (mContext != null) {
        sendCallback(mBannerCallback, true, "onBannerClicked");
      }
    }

    @Override
    public void onBannerFinished() {
      testLogDebug("onBannerFinished()");
      if (mContext != null) {
        sendCallback(mBannerCallback, true, "onBannerFinished");
      }
    }

    @Override
    public void onNoFill(Banner banner) {
      testLogError("onNoFill()");
      if (mContext != null) {
        sendCallback(mBannerCallback, false, "No fill");
      }
    }
  };

  private static void testLogDebug(String msg) {
    if (TEST_MODE) {
      Log.d(TAG, msg);
    }
  }

  private static void testLogError(String msg) {
    if (TEST_MODE) {
      Log.e(TAG, msg);
    }
  }
}
