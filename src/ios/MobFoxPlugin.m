//
//  MobFoxPlugin.m
//
//  Created by Shimon Shnitzer on 16/8/2016.
//  Updated and maintained by Andre Arsenault as of 13/11/2017.
//
//

#import "MobFoxPlugin.h"
#import <Cordova/CDV.h>
#import <MobFoxSDKCore/MobFoxSDKCore.h>

@interface MobFoxPlugin()
  @property (nonatomic,strong) MobFoxAd *banner;
@end

@implementation MobFoxPlugin

CDVInvokedUrlCommand *mBannerCommand = nil;
static const BOOL TEST_MODE = FALSE;
static const NSString *TESTING_HASH = @"fe96717d9875b9da4339ea5367eff1ec";

#define testLog(...) if (TEST_MODE) { NSLog(__VA_ARGS__); }


//======================================================================================
//======  G E N E R A L                                                           ======
//======================================================================================

- (void)pluginInitialize
{
  // Amazon MobAds GDPR legitimate interest flag.
  NSUserDefaults *userDefaults = NSUserDefaults.standardUserDefaults;
  [userDefaults setObject:@"1" forKey:@"aps_gdpr_pub_pref_li"];
}

- (void)dealloc
{
	if (self.banner != nil) {
		[self.banner removeFromSuperview];
		self.banner = nil;
	}
}

- (void)sendMobFoxCallback:(CDVInvokedUrlCommand *)command
               withSuccess:(BOOL)bOk 
                   andText:(NSString *)msg
{
  CDVPluginResult *pluginResult;
    
	if (bOk) {
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:msg];
  } else {
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:msg];
  }
  
  pluginResult.keepCallback = [NSNumber numberWithBool:TRUE];
  [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

//======================================================================================
//======  B A N N E R                                                             ======
//======================================================================================

- (void)showBanner:(CDVInvokedUrlCommand*)command
{
  testLog(@"MobFoxPlugin >> showBanner()");
  NSMutableDictionary *dict = [command.arguments objectAtIndex:0];

  if (dict == nil && [dict count] < 4) {
  	[self sendMobFoxCallback:command withSuccess:FALSE andText:@"Expected one dictionary parameter with at least 4 entries for showBanner()."];
    return;
  }

  NSString *invh = [dict objectForKey:@"hash"];
  NSString *position = [dict objectForKey:@"position"];
  NSNumber *bannerWidth = [dict objectForKey:@"bannerWidth"];
  NSNumber *bannerHeight = [dict objectForKey:@"bannerHeight"];
  
  if ((invh == nil) || (position == nil) || (bannerWidth == nil) || (bannerHeight == nil)) {
  	[self sendMobFoxCallback:command withSuccess:FALSE andText:@"Missing required parameters to showBanner(). "
      "Need: hash, position, bannerWidth, bannerHeight."];
    return;
  }

  mBannerCommand = command;
  if (self.banner == nil) {
    dispatch_async(dispatch_get_main_queue(), ^{
      [self createAndLoadBanner: invh
                       position: position
                    bannerWidth: [bannerWidth intValue]
                   bannerHeight: [bannerHeight intValue]];
    });
  } else {
    dispatch_async(dispatch_get_main_queue(), ^{
      [self loadBannerContent];
    });
  }
}

//======================================================================================

- (void)hideBanner:(CDVInvokedUrlCommand*)command
{
  testLog(@"MobFoxPlugin >> hideBanner()");
  if (self.banner != nil) {
    self.banner.hidden = TRUE;
  }
}

//======================================================================================

- (void)unhideBanner:(CDVInvokedUrlCommand*)command
{
  testLog(@"MobFoxPlugin >> unhideBanner()");
  if (self.banner != nil) {
    self.banner.hidden = FALSE;
  }
}

//======================================================================================

- (void)createAndLoadBanner:(NSString*)invh
                   position:(NSString*)position
                bannerWidth:(int)bannerWidth
               bannerHeight:(int)bannerHeight
{
  NSString *hash = TEST_MODE ? TESTING_HASH : invh;

  testLog(@"MobFoxPlugin >> createAndLoadBanner(invh:%@ position:%@ bannerWidth:%i bannerHeight:%i)",
    hash, position, bannerWidth, bannerHeight);

  CGRect bannerFrame = [self buildBannerFrameWithPosition:position
                                                    width:bannerWidth
                                                    height:bannerHeight];
  self.banner = [[MobFoxAd alloc] init:hash withFrame:bannerFrame];
  self.banner.delegate = self;
  [self.viewController.view addSubview:self.banner];

  [self loadBannerContent];
}

//======================================================================================

- (void)loadBannerContent
{
  testLog(@"MobFoxPlugin >> loadBannerContent()");
  [self.banner loadAd];
}

//======================================================================================

- (CGRect)buildBannerFrameWithPosition:(NSString *)position
                                 width:(int)width
                                height:(int)height
{
  // Expected values: (top|bottom)-(center|left|right)
  // Examples: bottom-center, top-left
  CGFloat x = 0.0f;
  CGFloat y = 0.0f;
  UIView *parentView = self.viewController.view;
  
  // Vertical properties (before the dash)
  if ([position containsString:@"bottom-"]) {
    y = parentView.frame.size.height - height;
  } else if (![position containsString:@"top-"]) {
    NSLog(@"MobFoxPlugin >> setBannerPosition: Unexpected vertical alignment "
          "parameter for position (%@), defaulting to top.", position);
  }

  // Horizontal properties (after the dash)
  if ([position containsString:@"-center"]) {
    x = (parentView.frame.size.width - width) / 2;
  } else if ([position containsString:@"-right"]) {
    x = parentView.frame.size.width - width;
  } else if (![position containsString:@"-left"]) {
    NSLog(@"MobFoxPlugin >> setBannerPosition: Unexpected horizontal alignment "
          "parameter for position (%@), defaulting to left.", position);
  }
  
  return CGRectMake(x, y, width, height);
}

//======================================================================================
// MobFox Ad Delegate

- (void)MobFoxAdDidLoad:(MobFoxAd *)banner
{
  testLog(@"MobFoxPlugin >> MobFoxAdDidLoad");
  [self sendMobFoxCallback:mBannerCommand withSuccess:TRUE andText:@"onBannerLoaded"];
}

- (void)MobFoxAdDidFailToReceiveAdWithError:(NSError *)error
{
  testLog(@"MobFoxPlugin >> MobFoxAdDidFailToReceiveAdWithError: %@", [error description]);
  NSString *msg = [NSString stringWithFormat:@"Unknown error"];
  if ((error != nil) && (error.userInfo != nil))
  {
    NSDictionary *errDict = [error.userInfo objectForKey:@"error"];
    if (errDict != nil)
    {
      msg = [NSString stringWithFormat:@"%@", errDict];
    }
  }

  [self sendMobFoxCallback:mBannerCommand withSuccess:FALSE andText:msg];
}

- (void)MobFoxAdClosed
{
  testLog(@"MobFoxPlugin >> MobFoxAdClosed");
  [self sendMobFoxCallback:mBannerCommand withSuccess:TRUE andText:@"onBannerClosed"];
}

- (void)MobFoxAdClicked
{
  testLog(@"MobFoxPlugin >> MobFoxAdClicked");
  [self sendMobFoxCallback:mBannerCommand withSuccess:TRUE andText:@"onBannerClicked"];
}

- (void)MobFoxAdFinished
{
  testLog(@"MobFoxPlugin >> MobFoxAdFinished");
  [self sendMobFoxCallback:mBannerCommand withSuccess:TRUE andText:@"onBannerFinished"];
}

@end
