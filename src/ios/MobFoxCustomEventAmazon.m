//
//  MobFoxCustomEventAmazon.m
//  MobFoxCoreDemo
//
//  Created by Shimon Shnitzer on 17/5/16.
//  Copyright © 2015 Shimon Shnitzer. All rights reserved.
//

#import "MobFoxCustomEventAmazon.h"
#import "MobFoxPlugin.h"

#import <AmazonAd/AmazonAdRegistration.h>
#import <AmazonAd/AmazonAdView.h>
#import <AmazonAd/AmazonAdOptions.h>
#import <AmazonAd/AmazonAdError.h>

static const BOOL TEST_MODE = FALSE;
#define testLog(...) if (TEST_MODE) { NSLog(__VA_ARGS__); }

@implementation MobFoxCustomEventAmazon

//========================================================================

- (UIViewController *)viewControllerForPresentingModalView
{
  return self.parentViewController;
}

- (void)adViewWillExpand:(AmazonAdView *)view
{
  testLog(@"MobFoxPlugin: Amazon >> adViewWillExpand");
  [self.delegate MFCustomEventMobFoxAdClicked];
}

- (void)adViewDidCollapse:(AmazonAdView *)view
{
  testLog(@"MobFoxPlugin: Amazon >> adViewDidCollapse");
}

- (void)adViewWillResize:(AmazonAdView *)view toFrame:(CGRect)frame
{
  testLog(@"MobFoxPlugin: Amazon >> adViewWillResize");
}

- (BOOL)willHandleAdViewResize:(AmazonAdView *)view toFrame:(CGRect)frame
{
  testLog(@"MobFoxPlugin: Amazon >> willHandleAdViewResize");
  return FALSE;
}

- (void)adViewDidFailToLoad:(AmazonAdView *)view withError:(AmazonAdError *)error
{
  testLog(@"MobFoxPlugin: Amazon >> adViewDidFailToLoad: %i:%@", error.errorCode, error.errorDescription);
  NSError *myErr = [NSError errorWithDomain:error.errorDescription
                                       code:error.errorCode
                                   userInfo:nil];
  [self.delegate MFCustomEventAdDidFailToReceiveAdWithError:myErr];
}

- (void)adViewDidLoad:(AmazonAdView *)view
{
  testLog(@"MobFoxPlugin: Amazon >> adViewDidLoad");
  [self.delegate MFCustomEventAd:self didLoad:view];
}

//========================================================================

- (void)requestAdWithSize:(CGSize)size
                networkID:(NSString*)networkId
          customEventInfo:(NSDictionary *)info
{
  testLog(@"MobFoxPlugin: Amazon >> requestAdWithSize()");

  self.parentViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];

  [[AmazonAdRegistration sharedRegistration] setAppKey:networkId];
  self.amazonAdView = [AmazonAdView amazonAdViewWithAdSize:size];
  self.amazonAdView.delegate = self;

  AmazonAdOptions *options = [AmazonAdOptions options];
  options.isTestRequest = TEST_MODE;
  [self.amazonAdView loadAd:options];
}

- (void)dealloc
{
  self.parentViewController = nil;
  self.amazonAdView.delegate = nil;
  self.amazonAdView = nil;
}

@end
