//
//  MobFoxPlugin.h
//
//  Created by Shimon Shnitzer on 16/8/2016.
//  Updated and maintained by Andre Arsenault as of 13/11/2017.
//
//

#import <Cordova/CDV.h>
#import <MobFoxSDKCore/MobFoxSDKCore.h>

@interface MobFoxPlugin : CDVPlugin <MobFoxAdDelegate>

- (void)pluginInitialize;
- (void)showBanner:(CDVInvokedUrlCommand *)command;
- (void)hideBanner:(CDVInvokedUrlCommand *)command;
- (void)unhideBanner:(CDVInvokedUrlCommand *)command;

@end
