//
//  MobFoxCustomEventAdMob.m
//  MobFoxCoreDemo
//
//  Created by Itamar Nabriski on 11/2/15.
//  Copyright © 2015 Itamar Nabriski. All rights reserved.
//

#import "MobFoxCustomEventAdMob.h"
#import "MobFoxPlugin.h"
#import "MFAdNetworkExtras.h"

static const BOOL TEST_MODE = FALSE;
#define testLog(...) if (TEST_MODE) { NSLog(__VA_ARGS__); }

@implementation MobFoxCustomEventAdMob

- (void)requestAdWithSize:(CGSize)size networkID:(NSString*)nid customEventInfo:(NSDictionary *)info
{
  testLog(@"MobFoxPlugin: AdMob >> requestAdWithSize()");

  CGRect rect = CGRectMake(0,0,size.width,size.height);
  self.bannerView = [[GADBannerView alloc] initWithFrame:rect];
  self.bannerView.delegate = self;
  self.bannerView.rootViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
  self.bannerView.adUnitID = nid;

  MFAdNetworkExtras *extras = [[MFAdNetworkExtras alloc] init];
  extras.gdpr = YES;
  extras.gdpr_consent = @"1";

  GADRequest* request = [GADRequest request];
  [request registerAdNetworkExtras:extras];
  if (TEST_MODE) {
    request.testDevices = @[ kGADSimulatorID,
                            @"8d4d2e668f2013e608a03285c3a9d362", // iPhone5
                            @"72de8f7fae3a07c6b6248d33c2b9f0e6" ]; // iPhone7+
  }

  [self.bannerView loadRequest:request];
}

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView
{
  testLog(@"MobFoxPlugin: AdMob >> adViewDidReceiveAd()");
  [self.delegate MFCustomEventAd:self didLoad:bannerView];
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error
{
  testLog(@"MobFoxPlugin: AdMob >> adViewDidFailToReceiveAdWithError(%@)", error);
  [self.delegate MFCustomEventAdDidFailToReceiveAdWithError:error];
}

- (void)adViewWillPresentScreen:(GADBannerView *)bannerView
{
  testLog(@"MobFoxPlugin: AdMob >> adViewWillPresentScreen()");
}

- (void)adViewDidDismissScreen:(GADBannerView *)bannerView
{
  testLog(@"MobFoxPlugin: AdMob >> adViewDidDismissScreen()");
  [self.delegate MFCustomEventAdClosed];
}

- (void)adViewWillDismissScreen:(GADBannerView *)bannerView
{
  testLog(@"MobFoxPlugin: AdMob >> adViewWillDismissScreen()");
}

- (void)adViewWillLeaveApplication:(GADBannerView *)bannerView
{
  testLog(@"MobFoxPlugin: AdMob >> adViewWillLeaveApplication()");
  [self.delegate MFCustomEventMobFoxAdClicked];
}

- (void)dealloc
{
  self.bannerView.delegate = nil;
  self.bannerView = nil;
}

@end
