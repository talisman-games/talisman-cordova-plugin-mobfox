# Cordova Plugin for Mobfox advertising

This plugin is based on the official [Mobfox Cordova Plugin](https://github.com/mobfox/SDK-Plugins/tree/master/Cordova/MobFoxCorPlugin).
I have cloned it here into its own repo because the original was usable
directly and is not maintained.

This plugin comes bundled with:

- [Mobfox iOS SDK](https://github.com/mobfox/MobFox-iOS-SDK):
  3.6.1
- [Mobfox Android SDK](https://github.com/mobfox/MobFox-Android-SDK): 3.6.9
- [Google AdMob Android SDK](https://developers.google.com/admob/android/quick-start): 17.2.0
- [Google AdMob iOS SDK](https://developers.google.com/admob/ios/rel-notes): 7.42.1
- [Amazon MobAds Android SDK](https://developer.amazon.com/sdk-download): 5.9.0
- [Amazon MobAds iOS SDK](https://developer.amazon.com/sdk-download): 2.2.17.0

## Intended Use

This plugin is intended for talisman.games internal use and as such we do not
accept issues, pull requests, or offer support. Feel free to fork this
repository for your own use in accordance with the [license](LICENSE).

## Using This Repository

To use this plugin in a Cordova-based application, add the plugin by
executing the following command-line:

```
cordova plugin add talisman-cordova-plugin-mobfox --variable ADMOB_APP_ID=[your admob app id]
```

Note that as of Google Mobile Ads SDK version 17.0.0 you must provide the `ADMOB_APP_ID`
variable when adding this plug-in on the command-line, otherwise you will get an error
while installing. If you update the Google Mobile Ads SDK without specifying this
variable on the command-line, your app will crash on launch.
See [this Google document](https://developers.google.com/admob/android/quick-start#update_your_androidmanifestxml)
for reference.
